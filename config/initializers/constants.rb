GRAPEFRUIT_VERSION_MAJOR = 0
GRAPEFRUIT_VERSION_MINOR = 5
GRAPEFRUIT_VERSION_PATCH = 8
GRAPEFRUIT_BUILD = `git rev-parse --short HEAD`
GITHUB_URL  =   'https://github.com/grapefruitinc/grapefruit'
CHANGELOG_URL = 'http://blog.grapefruit.link/tagged/changelog'
DATE_FORMAT_DAY = '%B %-d, %Y'
